# OJT用プロジェクトテンプレート


- [これはなに？](#これはなに？)
- [目的](#目的)
- [プロジェクト導入](#プロジェクト導入)
- [使い方](#使い方)
  - [計画](#計画)
  - [日報Issueの作成](#日報issueの作成)
  - [週報Issueの作成](#週報issueの作成)
  - [OJT報告書の作成](#ojt報告書の作成)
  - [運用フローのイメージ](#運用フローのイメージ)

## これはなに？

OJT用のプロジェクトです。  
このプロジェクトを用いることで、以下の事が実現可能です。

- 作業自己管理
- 日報管理
- 週報管理
- 学習事項のメモ
- 進捗状況の確認


## 目的

__「 教育サポートの手法の統一化 」__

OJTの運用は現場任せであり、それが最適な場合もあります。  
しかし繁忙期でのアサインなどで、トレーニーおよび現場責任者に余裕がない場合などに  
即時利用可能なベストプラクティスをテンプレートとして利用できる形で提供することや、  
`現場による教育サポートレベルの差を極力無くすための統一手法の提供`を目的とします。


__「 タスクの自己管理を学ぶ 」__

OJT報告書はトレーニーにとってはじめて自分自身が主管で進めるタスクになります。  
これと合わせて、日々の目標としていることや自分に割り当てられたタスクを  
自分で管理することで`社会人としてのタスク管理の基礎`を学んで頂きたいです。

__「 OJT情報の管理 」__

OJTをはじめとする成長の軌跡はとても重要な情報です。  
人材データの一環として同一の情報精度の元管理したいとも思います。  
同時にトレーナー及び組織の教育状況の確認にも利用したいです。  

## プロジェクト導入

GitLabのプロジェクト作成時に、Import機能を利用します。  
本プロジェクトのアーカイブを指定することで任意の名前でImport可能です。

### 初期設定

テンプレートプロジェクトには以下が含まれます。

- REASME.md
- Issue Template
- Issueのラベル定義
- サンプルのIssue
- サンプルのMilestone

サンプルOJTプロジェクトを確認出来る方は見ていただきたいですが、  
この手法ではIssueのBoard機能を使います。  
しかし、GitlabのExport機能では `Issue Board` の設定は引き継げません。  
従いまして、初期設定としてプロジェクト導入時に設定していただく必要があります。  

`Issue Board` は指定した `Label` を列として設定することができます。  
作成されたIssueに 列に対応する `Label` を設定することで表示されることになります。


![](images/gitlab-issue-board.png)

Issueの移動は、GUIでドラッグ&ドロップで直感的に扱えますのでメンテナンスが楽になります。


### トレーニーのプロジェクト作成

Import&初期設定したプロジェクトからFork機能を利用してトレーニー専用のプロジェクトを作りましょう。  
必要に応じてGroup / SubGroup を利用することも検討して下さい。  
Groupに対してアクセスできるメンバーの制御をきちんと行えばSubGroupはその設定を引き継ぐことができますので
設定する手間は少ないと思います。

例 : `OJT / 2019`

## 使い方

### 用語

|用語|意味|
|:---|:---|
|Milestone|物事の進捗を管理するために途中で設ける節目をいう。本テンプレートでは１週間をベースにしている。|
|Issue|本テンプレートではタスクを表す。|
|Label|Issueに対して様々な意味を付与するために利用する目印。|
|Board|Issueの管理を行いやすくするGUI。カンバンツールとも呼ばれる。|


### Issue Template の種類

本テンプレートにはいくつかの `Issue Template` が含まれます。 
それぞれ何に利用するのかを明確にします。  

|Issue Template|用途|
|:---|:---|
|Task|タスク定義として利用できるテンプレート|
|DailyReport|日報として利用できるテンプレート|
|WeeklyReport|週報として利用できるテンプレート|


### 計画

__「 Milestone 」__

OJTは計画から始まります。  
対象月に合わせて1週間毎にMilestoneを作成します。  
名称は `YYYY / MM - WeekN` が分かりやすいでしょう。  
MilestoneにIssueを登録することで進捗状況も簡単に確認することができます。

![](images/gitlab-issue-milestone.png)

__「 Issue 」__

各週毎に行うことを決めましょう。    
業務状況次第では計画が立てられる状況ではないこともあります。  
そうした場合にはタスクが明確になるまで無理にIssueを挙げる必要はありません。  

しかし、仮に空いたタイミングで自習等を進めるような場合には、  
自習という名目のIssueを挙げるようにしましょう。  
Issueを挙げる際には `Issue Template` に従って明確に記載しましょう。
以下の項目は入力して下さい。

- Assignee
- Milestone
- Due Date
- Label

### 日々の使い方

トレーニーは、日々自分が行うタスクを確認し状況を極力リアルタイムに更新しましょう。

__「 Issueのライフサイクル 」__

1. Issueを挙げる
1. Issueを実施すべきMilestone（週）になればBoardでWeekに移動する
1. Issueを実施すべき日になればBoardでDayに移動する
1. Issueを実施開始せたらBoardでWIPに移動する
1. トレーナーや業務上の責任者の確認が必要なIssueであればBoardでInRvに移動する（業務としての連絡は別途行って下さい）
1. 作業が完了、もしくは確認が完了したらIssueをCloseする


### 日報Issueの作成

日報としてIssueを書きましょう。  
Issue Templateの`「 DailyReport 」`を利用して下さい。  
Label の日報も忘れずにつけて下さい。  
Milestone の設定も忘れずにつけて下さい。  

この日報には、自分自身の簡易な覚え書きやトレーナーや諸先輩方への質問を書いてみても良いでしょう。  
質問をする場合には、IssueのAssigneeを質問を行いたい人に指名しておくと良いでしょう。

### 週報Issueの作成

週報としてIssueを書きましょう。  
Issue Templateの`「 WeeklyReport 」`を利用して下さい。  
Label の週報も忘れずにつけて下さい。  
Milestone の設定も忘れずにつけて下さい。  

この週報は、1週間の振り返りです。  
計画通り終わらなかったIssueについては扱いをトレーナーもしくは業務上の責任者と相談の上、翌週に回すのか実施をやめるのか決定して更新しましょう。  
またIssueの更新が疎かになっている場合は週報を書くタイミングでは最悪でも現状に合う形に更新して下さい。


### OJT報告書の作成

OJT報告書の作成時には、本プロジェクトを見返して振り返りながら記載すると良いでしょう。  
目標としたIssueを消化し達成できているのかを確認し、サマリーである週報をまとめることで報告書の内容は自ずとまとまるでしょう。  


### 運用フローのイメージ

![](images/ojt-project-template-flow.png)

